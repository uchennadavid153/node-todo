
## api url http://localhost:5000
<br>

# End points

<br>

## Authentication

### Register
#### url: http://localhost:5000/api/register 

```bash
curl -X POST -H 'Content-Type: application/json' 
        -d '{"username": "linuxize", "email": "linuxize@example.com", "password":'abcdefgh' }' \ 
        http://localhost:5000/api/register

```
### response 

```bash
{
  "code": "200",
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "user": {
      "newUser": {
        "email": "linuxize@example.com",
        "username": "linuxize",
        "_id": "6241905340bf49d9f1f9fa4a",
        "createdAt": "2022-03-28T10:39:15.643Z",
        "updatedAt": "2022-03-28T10:39:15.643Z",
        "__v": 0
      }
    }
  },
  "status": "",
  "message": "OK",
  "description": "indicates that the request has succeeded."
}

```

### Login
#### url: http://localhost:5000/api/login 

```bash
curl -X POST -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        -d '{ "email": "linuxize@example.com", "password":'abcdefgh' }' \ 
        http://localhost:5000/api/login

```
### response 

```bash
{
  "code": "201",
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  },
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```
### Get User
#### url: http://localhost:5000/api/getUser/:id

```bash
curl -X GET -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        http://localhost:5000/api/getUser/624195008ec62ee510a5a0d1

```

### response 
```bash
{
  "code": "201",
  "data": [],
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```

<br>

## Todos Operations
---
<br>

### Add Todo
#### url: http://localhost:5000/api/addTodo 

```bash
curl -X POST -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        -d '{ "title": "your title", "description":'your desription', "deadline":"13:00PM", "due_date":"2022/04/02" }' \ 
        http://localhost:5000/api/addTodo

```
### response 

```bash
{
  "code": "201",
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  },
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```

### Update Todo
#### url: http://localhost:5000/api/updateTodo/:todoId 

```bash
curl -X PUT -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        -d '{ "description":'your desription', "deadline":"13:00PM", "due_date":"2022/04/02", "is_completed":true }' \ 
        http://localhost:5000/api/updateTodo/624195008ec62ee510a5a0d1

```
### response 

```bash
{
  "code": "201",
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  },
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```


### Delete Todo
#### url: http://localhost:5000/api/deleteTodo/:todoId 

```bash
curl -X DELETE -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        http://localhost:5000/api/deleteTodo/624195008ec62ee510a5a0d1

```
### response 

```bash
{
  "code": "201",
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  },
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```


### Get All Todo
#### url: http://localhost:5000/api/getTodo/

```bash
curl -X GET -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        http://localhost:5000/api/getTodo

```

### response 
```bash
{
  "code": "201",
  "data": [],
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```


### Get Single Todo
#### url: http://localhost:5000/api/getSingleTodo/:id

```bash
curl -X GET -H 'Content-Type: application/json' 'Authorization: "Bearer token"' 
        http://localhost:5000/api/getTodo/624195008ec62ee510a5a0d1

```

### response 
```bash
{
  "code": "201",
  "data": [],
  "status": "",
  "message": "Created",
  "description": "indicates that the request has been fulfilled and has resulted in one or more new resources being created."
}
```


# commands 

```bash
--- clone with https

git clone https://gitlab.com/uchennadavid153/node-todo.git

--- clone with SSH

git clone git@gitlab.com:uchennadavid153/node-todo.git

cp .env.example .env && cd src

mkdir environments && cp response.js environments/response.js

node helpers/cryptographic.helper.js

npm install [use -f (if required) ]

sudo rm -r response.js (if using linux) || del "response.js"

npm run start

```
