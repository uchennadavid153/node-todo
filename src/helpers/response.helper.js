import {data} from "../environments/response";
import {response} from 'express'
export class ResponseHelper {
  responseObject = {};
  constructor(status_code, dataObject, otherData = null) {
      try {
          this.responseObject = data.find((a) => a?.code === status_code);

          this.responseObject["data"] = dataObject;
          !!otherData && (this.responseObject["other_data"] = otherData);
          return this.responseObject

      } catch (error) {

        return error
      }
  }


  
}
