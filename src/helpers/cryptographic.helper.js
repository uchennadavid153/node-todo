
import { generateKeyPairSync  } from "crypto";
import { writeFileSync } from "fs";
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

let {publicKey, privateKey } = generateKeyPairSync("rsa", {
  modulusLength: 4096,
  publicKeyEncoding: {
    type: "pkcs1",
    format: "pem",
  },

  privateKeyEncoding: {
    type: "pkcs1",
    format: "pem",
  },
});

writeFileSync(
   `${__dirname}/../environments/pub.pem`,
  publicKey.toString()
);

writeFileSync(
   `${__dirname}/../environments/priv.pem`,
  privateKey.toString()
);
