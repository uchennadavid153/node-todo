import express, { urlencoded, json } from "express";
import cors from "cors";
import  './config/database.config';
import { config } from "dotenv";
import {} from './helpers/response.helper'
import route from './routes/api.route'

config();
const app = express();
const port = process.env.PORT || 8000;


app.use(cors());
app.use(urlencoded({ extended: true }));
app.use(json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type, Authorization"
  );
  next();
});

process.on("uncaughtException", (err) => {
  console.error(JSON.stringify({
    message: err.message,
    stack: err.stack,
    name: err.name,
  }));
  
  process.exit(1)
});

process.on("unhandledRejection", (err) => {
console.error(JSON.stringify({
  message: err.message,
  stack: err.stack,
  name: err.name,
  state:"=== UNHANDLED REJECTION ==="
}));
process.exit(1);
});

app.use(route);
app.listen(port, () =>
  console.log(`app is listerning on http://${process.env.HOST}:${port}`)
);
