import userModel from "../models/user.model";
import {ResponseHelper} from './../helpers/response.helper'

export class HomeController {
    async home(req, res){
        try {
            return res.json(new ResponseHelper("200", {"message":"welcome to express todo api version 1"}))
        } catch (error) {
            return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
        }
    }
    
    async getUser(req, res){
        try {
            if(!req?.params?.user_id) return res.json(new ResponseHelper("400", {"error":"USER ID NOT FOUND!"}))
            let user = await userModel.findById(req?.params?.user_id)
            delete user['password'];
            return res.json(new ResponseHelper("200", {"user":user}))
        } catch (error) {
            return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
        }
    }

}