import todoModel from "../models/todo.model";
import {refresh_token} from "../utils/signtoken.util"
import {ResponseHelper} from './../helpers/response.helper'

export class CrudController {
  /**
   * add todo controller
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */

  async addTodo(req, res, next) {
    try {
     if(!req.body ) return res.json(new ResponseHelper("400", {"error":"BADLY FORMATED"}));

      let new_todo = {"user_id":req?.res?.id};
 
      for (let [key, value] of Object.entries(req.body))
        !!value && (new_todo[key] = `${value.trim()}`);

      await todoModel.create(new_todo);

      let token = await refresh_token({_id:req?.res?.id, email:req?.res?.email})
      // 201 status code here
     return res.json( new ResponseHelper("201", {token:token}))

    } catch (error) {
     return res.json( new ResponseHelper("500", {"error":JSON.stringify(error)}))
    }
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */

  async updateTodo(req, res, next) {
    try {

      if(!req.body || !req?.params) return  res.json(new ResponseHelper("400", {"error":"BADLY FORMATED"}));

      let update_todo = {};

      for (let [key, value] of  Object.entries(req.body))
        !!value && (update_todo[key] = `${value}`);

      await todoModel.findByIdAndUpdate(req?.params?.id, {$set:update_todo})

      let token = await refresh_token({_id:req?.res?.id, email:req?.res?.email})

      return res.json(new ResponseHelper("201", {token:token}));

    } catch (error) {
      return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
    }
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */

  async deleteTodo(req, res, next) {
    try {
      if(!req?.params?.id) return res.json(new ResponseHelper("400", {"error":"BADLY FORMATED"}));

      await todoModel.findByIdAndDelete(req?.params?.id);

      let token = await refresh_token({_id:req?.res?.id, email:req?.res?.email})

      // 201 status code here
      return res.json(new ResponseHelper("201", {token:token}))

    } catch (error) {
      return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
    }
  }

  /**
   * 
   * @param {*} req 
   * @param {*} res 
   * @param {*} next 
   */

async getTodo(req, res, next){
try {

    if(!req.res || !req.res.id) return res.json(new ResponseHelper("400", {"error":"HEADERS NOT FOUND"}));
    
    let todos = await todoModel.find({user_id:req.res.id});

   return res.json( new ResponseHelper("200", todos))
    // 
} catch (error) {
    return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
}
}

async getSingleTodo(req, res, next){
  try {
  
      if(!req.res || !req.res.id) return res.json(new ResponseHelper("400", {"error":"HEADERS NOT FOUND"}));
      
      let todos = await todoModel.findById(req.params.id);

     return res.json( new ResponseHelper("200", todos))
      // 
  } catch (error) {
      return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
  }
  }
}
