import userModel from "../models/user.model";
import { access_token, refresh_token } from "../utils/signtoken.util";
import { hashPassword, verifyHash } from "../utils/encrypt_decrypt.util";
import {ResponseHelper} from './../helpers/response.helper'

export class AuthController {
  async register(req, res, next) {
    try {
      if(!req?.body) return res.json(new ResponseHelper("400", {"error":"BADLY FORMATEd"}));

      const { email, password, username } = req?.body;

      if(!email || !password || !username) return res.json(new ResponseHelper("400", {"error":"BADLY FORMATED"}));

      const user = await userModel.findOne({email: email});

      if(user) return res.json(new ResponseHelper("422", {"error":"USER WITH THIS EMAIL EXIST"}));

      let userObject = {
        email: email.trim(),
        username: username.trim(),
        password: hashPassword(password),
      };

      let newUser = await userModel.create(userObject);

      let token = await access_token({
        _id: newUser?._id,
        email: newUser?.email,
      });

      res.json(new ResponseHelper("200", {token:token, user:{newUser}}))
      // return data
    } catch (error) {
      res.json(new ResponseHelper("400",{"error":JSON.stringify(error)} ))
    }
  }

  async login(req, res, next) {
    try {
      if(!req?.body )return res.json(new ResponseHelper("400", {"error":"BADLY FORMATEd"}));

      let { email, password } = req?.body;

      if(!email || !password) return res.json(new ResponseHelper("422",{"error":"DATA BADLY FORMATED"} ));

      let user = await userModel.findById(req.res.id);

      let verify = verifyHash(password, user?.password);

      if (!user) return res.json(new ResponseHelper("400",{"error":"USER NOT FOUND"} ));

      if (!verify || user?.email !== email) return res.json(new ResponseHelper("400",{"error":"INVALID EMAIL OR PASSWORD"} ));

      let token = await refresh_token({
        _id: user?._id,
        email: user?.email,
      });

      res.json(new ResponseHelper("200", {token:token, user}))
      // return data
    } catch (error) {
       res.json( new ResponseHelper("500",{"error":error} ) )
    }
  }

  
}
