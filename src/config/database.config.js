import mongoose from 'mongoose';
import {config} from 'dotenv'
config()

export const connectDatabase = async () => {
    try {
       await mongoose.connect(process.env.DB_URL)
    } catch (error) {
        throw new Error(error.message)
    }
}

connectDatabase().then(()=>console.log('connected')).catch(err => console.log(err))
