import jwt from "jsonwebtoken";
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import { readFileSync } from "fs";
import {ResponseHelper} from '../helpers/response.helper'

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

  const PUBLIC_KEY = readFileSync(
    `${__dirname}/../environments/pub.pem`,
    "utf8"
  );

export const authenticateToken = async ( req, res, next ) => {
    try {
      const authHeader = req.headers["authorization"];

      const token = authHeader && authHeader.split(" ")[1];

      if(!token ) return res.json(new ResponseHelper("400", {"error":"TOKEN NOT FOUND"}));
        
     jwt.verify(token,PUBLIC_KEY,{ algorithms: ["RS256"], ignoreExpiration: false }, (err, user) => {

          if(!!err) return  res.json(new ResponseHelper("400", {"error":JSON.stringify(err)}));
      
          const {exp, ...others} = user;
         
          if(!timer(exp)) return  res.json(new ResponseHelper("422", {"error":"TOKEN EXPIRED"}));

          req.res = others;

          next();
      })

    } catch (error) {
      return res.json(new ResponseHelper("500", {"error":JSON.stringify(error)}))
    }
  };

  const timer = (exp) => {
    const now =  Date.now();
    if(exp - now > 0 ) return true
    return false
  };
  