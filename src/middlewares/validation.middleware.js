import { ResponseHelper } from "../helpers/response.helper";

let nameRegex = /^[a-zA-Z]{3,}$/i;
const emailRegex =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export let registerMiddleWare = (val, res, next) => {
  let { username, email, password } = val.body;
  switch (false) {
    case nameRegex.test(capitalizer(username)):
      res.json(new ResponseHelper("400", {
        "error": "first name badly formated",
      }));
      break;

    case emailRegex.test(email.trim()):
      res.json(new ResponseHelper("400", {
        "error": "email badly formatted",
      }));
      break;

    case (password.length > 6):
     res.json( new ResponseHelper("400", {
      "error": "password must be more than 6 xters",
    }))
      break;

    default:
      val.body = {
        username: capitalizer(username),
        email: email.trim(),
        password: password,
      };
      next();
      break;
  }
};

export let loginMiddleware = (val, res, next) => {
  let { email, password } = val.body;
  switch (false) {
    case emailRegex.test(email.trim()):
      res.status(new ResponseHelper("400", {
        "error": "email badly formatted",
      }));
      break;

    case password.toString().length > 6:
     res.json( new ResponseHelper("400", {
      "error": "password must be more than 6 xters",
    }));
      break;

    default:
      val.body = {
        email: email.trim(),
        password: password,
      };
      next();
      break;
  }
};


const capitalizer = (v) => v.trim().charAt(0).toUpperCase() + v.trim().slice(1);
