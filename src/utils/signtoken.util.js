import { fileURLToPath } from 'url';
import { dirname } from 'path';
import { readFileSync } from "fs";
import jwt from "jsonwebtoken";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const PRIVATE_KEY = readFileSync(
    `${__dirname}/../environments/priv.pem`,
    "utf8"
  );
  
  const signToken = async (data, exp) => {
    try {
      const payload = {
        id: data?._id,
        email:data?.email,
        iat: Date.now(),
        exp: Date.now() + exp
      };
      const signedToken =jwt.sign(payload, PRIVATE_KEY, {
        algorithm: "RS256",
      });
      return signedToken;
    } catch (error) {
      return false;
    }
  };

 const access_token =async (data) => await signToken(data, 86400000);
 const refresh_token =async (data) =>await signToken(data, 300000);

export {
    access_token, refresh_token
}