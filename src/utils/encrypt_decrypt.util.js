import { randomBytes, pbkdf2Sync } from "crypto";

export const hashPassword = (password) => {
    const salt = randomBytes(16).toString('hex')
    const hash =  pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex')
    return [salt, hash].join('$$')
};

export const verifyHash = (password, original) => {
    const originalHash = original.split('$$')[1];
    const salt = original.split('$$')[0];
    let hash = pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex')

    if(originalHash !== hash){
        return false
    }else{
        return true
    }
  
};
