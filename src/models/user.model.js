import mongoose from 'mongoose'

/**
 * schema Todo {title, description, deadline}
 */

const user = new mongoose.Schema({
  email:{
      type:String,
      required:true
  },
  password:{
      type:String,
      required:true
  },
  username:{
      type:String,
      required:true
  },

},{
    timestamps:true
})

export default mongoose.model('User', user);
