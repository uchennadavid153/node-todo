import mongoose from 'mongoose'

/**
 * schema Todo {title, description, deadline}
 */
const Todo = new mongoose.Schema({
  title:{
      type:String,
      required:true
  },
  user_id:{
    type:String,
    required:true
},
  description:{
      type:String,
      required:true
  },
  deadline:{
      type:String,
      required:true
  },
  due_date: {
      type:String,
      required:true
  },
  is_completed:{
      type:Boolean,
      default:false
  }
},{
    timestamps:true
})

export default mongoose.model('Todo', Todo);
