import { Router } from "express";
import {
  loginMiddleware,
  registerMiddleWare,
} from "../middlewares/validation.middleware";
import { authenticateToken } from "../middlewares/auth.middleware";
import { CrudController } from "../controllers/crud.controller";
import { AuthController } from "../controllers/auth.controller";
import {HomeController} from '../controllers/home.controller'

let { register, login } = new AuthController();
let { addTodo, updateTodo, deleteTodo, getTodo, getSingleTodo } = new CrudController();
let { getUser, home } = new HomeController();
let route = Router();

route.post("/api/register", [registerMiddleWare], register);
route.post("/api/login", [loginMiddleware, authenticateToken], login);
route.post("/api/addTodo", [authenticateToken], addTodo);

route.put("/api/updateTodo/:id", [authenticateToken], updateTodo);

route.delete("/api/deleteTodo/:id", [authenticateToken], deleteTodo);

route.get("/api/getTodo", [authenticateToken], getTodo);
route.get('/api/getUser/:user_id', [authenticateToken], getUser)
route.get('/api/getSingleTodo/:id', [authenticateToken], getSingleTodo)
route.get('/api', home );

export default route;
